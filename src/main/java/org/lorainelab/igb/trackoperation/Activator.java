/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.trackoperation;

import aQute.bnd.annotation.component.Activate;
import aQute.bnd.annotation.component.Component;
import aQute.bnd.annotation.component.Deactivate;
import com.affymetrix.genometry.operator.Operator;
import com.affymetrix.genometry.parsers.FileTypeCategory;
import java.util.ArrayList;
import java.util.List;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

/**
 *
 * @author noorzahara
 */
@Component(immediate = true)
public class Activator  {

    private BundleContext bundleContext;
    private final List<ServiceReference<Operator>> serviceReferences;

    public Activator() {
        serviceReferences = new ArrayList<>();
    }
    
     @Activate
    public void activate(BundleContext bundleContext) {
        this.bundleContext = bundleContext;
        bundleContext.registerService(Operator.class, new SoftClipDepthOperator(FileTypeCategory.Alignment), null);
    }
    
    @Deactivate
    public void deactivate() {
        serviceReferences.forEach(sr -> bundleContext.ungetService(sr));
    }
    
}