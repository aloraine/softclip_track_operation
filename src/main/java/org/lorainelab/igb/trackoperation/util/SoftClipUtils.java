/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lorainelab.igb.trackoperation.util;

import cern.colt.list.FloatArrayList;
import cern.colt.list.IntArrayList;
import com.affymetrix.genometry.BioSeq;
import com.affymetrix.genometry.GenomeVersion;
import com.affymetrix.genometry.SeqSpan;
import com.affymetrix.genometry.symmetry.impl.BAMSym;
import com.affymetrix.genometry.symmetry.impl.GraphIntervalSym;
import com.affymetrix.genometry.symmetry.impl.GraphSym;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;
import static com.affymetrix.genometry.util.SeqUtils.isBamSym;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author noorzahara
 */
public class SoftClipUtils {
    
    public static final GraphSym getSymmetrySoftclipSummary(List<SeqSymmetry> syms, BioSeq seq, boolean binary_depth, String id) {
        int symcount = syms.size();
        List<SeqSpan> leaf_spans = new ArrayList<>(symcount);
        for (SeqSymmetry sym : syms) {
            collectSoftclipSpans(sym, seq, leaf_spans);
        }
        if (leaf_spans.isEmpty()) {
            return null;
        } else {
            return getSpanSummary(leaf_spans, binary_depth, id);
        }
    }
    
    /**
     * GetSpanSummary. General idea is that this will make getUnion(),
     * getIntersection(), etc. easier and more efficient.
     *
     * @param spans a List of SeqSpan's all defined on the same BioSeq
     * @param binary_depth if false, then return a graph with full depth
     * information if true, then return a graph with flattened / binary depth
     * information, 1 for covered, 0 for not covered
     */
    public static final GraphIntervalSym getSpanSummary(List<SeqSpan> spans, boolean binary_depth, String gid) {
        BioSeq seq = spans.get(0).getBioSeq();
        int span_num = spans.size();
        int[] starts = new int[span_num];
        int[] stops = new int[span_num];
        for (int i = 0; i < span_num; i++) {
            SeqSpan span = spans.get(i);
            starts[i] = span.getMin();
            stops[i] = span.getMax();
        }
        Arrays.sort(starts);
        Arrays.sort(stops);
        int starts_index = 0;
        int stops_index = 0;
        int depth = 0;
        int max_depth = 0;
        // initializing capacity of sum_starts and sum_stops to max that could theoretically be
        //   needed, though likely won't fill it
        IntArrayList transition_xpos = new IntArrayList(span_num * 2);
        FloatArrayList transition_ypos = new FloatArrayList(span_num * 2);

        int prev_depth = 0;
        while ((starts_index < span_num) && (stops_index < span_num)) {
            // figure out whether next position is a start, stop, or both
            int next_start = starts[starts_index];
            int next_stop = stops[stops_index];
            int next_transition = Math.min(next_start, next_stop);
            // note that by design, if (next_start == next_stop), then both of the following
            //    conditionals will execute:
            if (next_start <= next_stop) {
                while ((starts_index < span_num) && (starts[starts_index] == next_start)) {
                    depth++;
                    starts_index++;
                }
            }
            if (next_start >= next_stop) {
                while ((stops_index < span_num) && (stops[stops_index] == next_stop)) {
                    depth--;
                    stops_index++;
                }
            }
            if (binary_depth) {
                if ((prev_depth <= 0) && (depth > 0)) {
                    transition_xpos.add(next_transition);
                    transition_ypos.add(1);
                    prev_depth = 1;
                } else if ((prev_depth > 0) && (depth <= 0)) {
                    transition_xpos.add(next_transition);
                    transition_ypos.add(0);
                    prev_depth = 0;
                }
            } else {
                transition_xpos.add(next_transition);
                transition_ypos.add(depth);
                max_depth = Math.max(depth, max_depth);
            }
        }
        // clean up last stops...
        //    don't need to worry about "last starts", all starts will be done before last stop...
        while (stops_index < span_num) {
            int next_stop = stops[stops_index];
            int next_transition = next_stop;
            while ((stops_index < span_num) && (stops[stops_index] == next_stop)) {
                depth--;
                stops_index++;
            }
            if (binary_depth) {
                if ((prev_depth <= 0) && (depth > 0)) {
                    transition_xpos.add(next_transition);
                    transition_ypos.add(1);
                    prev_depth = 1;
                } else if ((prev_depth > 0) && (depth <= 0)) {
                    transition_xpos.add(next_transition);
                    transition_ypos.add(0);
                    prev_depth = 0;
                }
            } else {
                transition_xpos.add(next_transition);
                transition_ypos.add(depth);
                max_depth = Math.max(depth, max_depth);
            }
        }
        transition_xpos.trimToSize();
        transition_ypos.trimToSize();
        int[] x_positions = transition_xpos.elements();
        int[] widths = new int[x_positions.length];
        for (int i = 0; i < widths.length - 1; i++) {
            widths[i] = x_positions[i + 1] - x_positions[i];
        }
        widths[widths.length - 1] = 1;
        // Originally, this returned a GraphSym with just x and y, but now has widths.
        // Since the x and y values are not changed, all old code that relies on them
        // does not need to change.
        String uid = GenomeVersion.getUniqueGraphID(gid, seq);
        GraphIntervalSym gsym
                = new GraphIntervalSym(x_positions, widths, transition_ypos.elements(), uid, seq);
        return gsym;
    }
    
    /**
     * Traverse sym, populate passed list with soft-clip regions of reads.
     * @param sym
     * @param seq
     * @param leafs
     */
    /**
      * This method populate the leafs collection with only soft-clip portions of aligned reads.
      * Please go through Jira ticket IGBF-1443 for complete sequence of added methods 
      *  for implementing Depth Graph(Soft-clip) feature.
    */
    public static final void collectSoftclipSpans(SeqSymmetry sym, BioSeq seq, Collection<SeqSpan> leafs) {
  
        int childCount = sym.getChildCount();
        if (childCount == 0) {
            SeqSpan span = sym.getSpan(seq);
            if (span != null) {
                leafs.add(span);
            }
        } else {
            for (int i = 0; i < childCount; i++) {
                //Checking if SeqSymmetry is of type BamSym(BamSym only have soft clips)
                if (isBamSym(sym)){
                    //Typecasting SeqSymmetry to BAMSym
                    BAMSym bamSym = (BAMSym)sym;
                    for(int ii = 0; ii < bamSym.getSoftChildCount(); ii++){
                        collectSoftclipSpans(bamSym.getSoftChild(ii), seq, leafs);
                    }
                } else{
                    collectSoftclipSpans(sym.getChild(i), seq, leafs);
                }
            }
        }
    }
    
}
